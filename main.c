#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "SHA-256.h"

#ifndef SET_WORD_VAL    // precise integer length is required for the SHA algorithm
#define SET_WORD_VAL
typedef uint32_t WORD;  // WORD is a 32bit unsigned int
typedef uint64_t WORD64;    // WORD64 is a 64bit unsigned int
#endif

// Global variables
WORD64 size = 0;    // size of the hashed data in bytes
char *msg = NULL;   // text to hash, or the current chunk when hashing a file
unsigned char *hash = NULL;
char *path = NULL;  // path to file to hash
int verbose = 0;    // verbose flag


long my_strtol(const char *in){     // converts string to long, returns -1 on error
    unsigned long output = 0;
    int i = 0;
    for (i = 0; i < size; i++) {
        if (isdigit(in[i])){    // if current char is a digit, adds it to the end of output
            output *= 10;
            output += in[i]-'0';
        }
        else
            return -1;  // return -1, if a char in the input is not a digit
    }
    return output;
}

char *readText(char *message){  // reads text char-by-char, outputs a char array WITHOUT '\0'
    char *result = NULL;  // array to store the output
    char c; // current char
    char* new;  // new array
    size = 0;
    printf("%s",message);   // print out a message
    while ( (c = (char) getchar()) != '\n') {   // read until newline
        new = malloc(sizeof(char) * size + 1);  // allocate new array
        memset(new, 0, size + 1);   // fill it with 0
        if ( result != NULL)
            memcpy(new, result, sizeof(char) * size);   // copy old, if it exists
        new[size++] = c;    // add current char
        if ( result != NULL)
            free(result);   // free old if exists
        result = new;   // new is old
    }

    return result;
}
char randChar(){
    return (char) (((rand()/(RAND_MAX+1.0)) * 94) + 33);    // returns a random printable ascii char
}

void salt(long len){   // adds a random string with length 'len' to the end of msg
    int i;
    if (len < 0)    // len shouldn't be negative
        return;
    char *result = (char *) malloc((size_t)len);    // allocate output array

    printf("Salt string: ");
    for (i = 0; i < len; i++) { // generate and print out rand chars
        result[i] = randChar();
        printf("%c", result[i]);
    }
    printf("\n");

    msg = realloc(msg, size+len);   // enlarge msg
    for (i = 0; i < len; i++)
        msg[i+size] = result[i];    // copy salt
    size += len;
    free(result);
}

int chooseMode(){   // basic menu
    char  mode;
    char *temp;
    long len;

    printf("Choose operating mode:\n");
    printf("(0) Text input\n");
    printf("(1) File input\n");
    printf("(q) Exit program\n");

    temp = readText("");
    mode = temp[0];
    free(temp);

    switch (mode) {
        case '0':
            printf("\nDo you want to salt the text?\n");
            printf("(0) No\n");
            printf("(length of salt) Yes\n");

            temp = readText("");
            len = my_strtol(temp);
            free(temp);

            if (len == -1){
                printf("No, that was not a number, try again.\n");
                return -1;
            }

            if (!len)
                msg = readText("Please enter text: ");
            else {
                msg = readText("Please enter text: ");
                salt(len);
            }
            return 2;
        case '1':
            verbose = 1;
            path = readText("Enter file path: ");
            return 1;
        case 'q':   // LET ME OUT OF THIS DEVIOUS LOOP
            return 0;
        default:
            printf("Invalid mode!\n");
            return -1;
    }
 }

void printHelp(char *execName){   // prints help message
     printf("Usage:\n\n"
            "-f|--file   \tenter file path\n"
            "-t|--text   \tenter text to hash\n"
            "-s|--salt   \tenter salt size\n"
            "-v|--verbose\tverbose file hashing (ex. progress messages)\n"
            "-h|--help   \tdisplay this help message\n\n"
            "-f and -t are mutually exclusive!\n"
            "-s requires text input\n"
            "-v only affects file hash\n\n"
            "ex.:\t%s -f /path/to/file\n"
            "    \t%s -t \"Text to hash\" -s 16\n\n",execName,execName);
    if(path != NULL) free(path);
    if(msg != NULL) free(msg);
 }

int main(int argc, char *argv[]){
    int i;
    int mode = 0;   // -1:invalid 0:exit 1:file 2:text
    int pos = 1;    // 0th is exec name
    long saltLen = 0;
    unsigned temp;

    srand((unsigned int) time(NULL));

    if (argc == 1) {    // if no args, call chooseMode
        while ((mode = chooseMode()) == -1);  // loop until valid input
        if (mode == 0)
            return 0;   // close program, if exit is selected
    }

    else {

        while ( pos < argc ){   // go through all args
            if ( !strcmp("-h",argv[pos]) || !strcmp("--help",argv[pos]) ){
                printHelp(argv[0]);
                return 0;
            }
            else if( !strcmp("-t",argv[pos]) || !strcmp("--text",argv[pos]) ){
                pos++;
                if (pos == argc) {
                    printHelp(argv[0]);
                    return 0;
                }
                size = strlen(argv[pos]);
                msg = (char *) malloc(size+1);
                strcpy(msg,argv[pos]);
                pos++;
                mode = 2;
            }
            else if( !strcmp("-s",argv[pos]) || !strcmp("--salt",argv[pos]) ){
                pos++;
                if (pos == argc) {
                    printHelp(argv[0]);
                    return 0;
                }
                saltLen = my_strtol(argv[pos]);
                if (saltLen == -1) {
                    printHelp(argv[0]);
                    return 0;
                }
                pos++;
            }
            else if( !strcmp("-v",argv[pos]) || !strcmp("--verbose",argv[pos]) ){
                pos++;
                verbose = 1;
            }
            else if( !strcmp("-f",argv[pos]) || !strcmp("--file",argv[pos]) ){
                pos++;
                if (pos == argc) {
                    printHelp(argv[0]);
                    return 0;
                }
                temp = (unsigned) strlen(argv[pos]);
                path = malloc(temp+1);
                strcpy(path,argv[pos]);
                pos++;
                mode = 1;
            }
            else {
                printf("Invalid argument!\n");
                printHelp(argv[0]);
                return 0;
            }
        }

    }
    if ( mode == 1 ){
        hash = sha256file(path,verbose);
    }
    else if( mode == 2){
        if(saltLen)
            salt(saltLen);
        hash = sha256(msg,size);
    }
    else {
         printf("No valid input was provided\n\n");
         printHelp(argv[0]);
    }

    if (hash == NULL) {
        return -1;
    }

    for (i = 0; i < 32; i++) {
        printf("%02x", hash[i]);     // print the hash in hex
    }
    if (mode == 1)
        printf("  %s\n",path);
    else
        printf("\n");
    free(hash);
    free(path);
    return 0;
}